package repositories

import (
	"gitlab.com/prajnapras19/go-api-template/models"
)

type ExampleRepository interface {
	GetMessage() models.Example
}

type exampleRepositoryImpl struct {

}

func NewExampleRepositories() ExampleRepository {
	return &exampleRepositoryImpl{}
}

func (r *exampleRepositoryImpl) GetMessage() models.Example {
	return models.Example{"Hello, world!"}
}