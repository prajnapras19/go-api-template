package controllers

import (
	"gitlab.com/prajnapras19/go-api-template/services"
	"github.com/gin-gonic/gin"

	"net/http"
)

type ExampleController interface {
	GetExample(c *gin.Context)
}

type exampleController struct {
	service services.ExampleService
}

func NewExampleController(service services.ExampleService) ExampleController {
	return &exampleController{service}
}

// GetExample is example endpoint
// @Summary 		Example endpoint
// @Description 	Just an example endpoint
// @Tags 			example
// @Accept 			json
// @Produce  		json
// @Success 		200
// @Router 			/ [get]
func (c *exampleController) GetExample(gc *gin.Context) {
	gc.JSON(http.StatusOK, c.service.Example())
}