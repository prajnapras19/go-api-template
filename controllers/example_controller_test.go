package controllers

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
	mocks "gitlab.com/prajnapras19/go-api-template/mocks/services"
	"gitlab.com/prajnapras19/go-api-template/models"
)

func setupRouter(controller ExampleController) *gin.Engine {
	router := gin.Default()
	router.GET("/", controller.GetExample)
	return router
}

func TestGETExampleReturnsHello(t *testing.T) {	
	service := new(mocks.ExampleService)
	service.On("Example").Return(models.Example{"Hello, world!"})

	controller := NewExampleController(service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/", nil)

	router := setupRouter(controller)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "{\"message\":\"Hello, world!\"}", w.Body.String())
}
