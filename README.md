# Go Template API

We can consider it as a template for our API projects with Go.

## How To Run
- Go to project root directory (the directory with `main.go`), and run:
    ```
    go run main.go
    ```
- To format your code from project root directory:
    ```
    go fmt ./...
    ```

## How To Test
- Go to your package directory, and run:
    ```
    go test . -v
    ```
- To get coverage:
    ```
    go test . -cover
    ```
- To get which part of code have been covered or not:
    ```
    go test . -coverprofile coverage.cov
    go tool cover -html=coverage.cov
    ```
- If you want to test or get coverage for all package, go to project root directory, and run
    ```
    go test ./... -v
    ```

## Swagger API Integration
- Install it first:
    ```
    go install github.com/swaggo/swag/cmd/swag@latest
    ```
- To generate docs
    ```
    swag init -parseDependency=true
    ```

- Accessing swagger ui
    ```
    http://localhost:8080/swagger/index.html
    ```

## Create Mock
- Install it first:
    ```
    go install github.com/vektra/mockery/v2@latest
    ```
- From the project root directory, run:
    ```
    mockery --all --keeptree
    ```