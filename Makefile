dep:
	go get -v -d ./...
build:
	go build -o main -v .
test:
	go test -short ./...
coverage:
	go test ./... -coverprofile coverage.cov;
	go tool cover -func=coverage.cov;