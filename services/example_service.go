package services

import (
	"gitlab.com/prajnapras19/go-api-template/models"
	"gitlab.com/prajnapras19/go-api-template/repositories"
)

type ExampleService interface {
	Example() models.Example
}

type exampleServiceImpl struct {
	repository repositories.ExampleRepository
}

func NewExampleService(repository repositories.ExampleRepository) ExampleService {
	return &exampleServiceImpl{repository}
}

func (s *exampleServiceImpl) Example() models.Example {
	return s.repository.GetMessage()
}
