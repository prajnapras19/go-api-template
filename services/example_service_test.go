package services

import (
	mocks "gitlab.com/prajnapras19/go-api-template/mocks/repositories"
	"gitlab.com/prajnapras19/go-api-template/models"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestExampleServiceReturnsExampleMessage(t *testing.T) {
	exampleRepositoryMock := new(mocks.ExampleRepository)
	exampleRepositoryMock.On("GetMessage").Return(models.Example{"test"})
	
	exampleService := NewExampleService(exampleRepositoryMock)

	res := exampleService.Example()
	assert.Equal(t, "test", res.Message)
}