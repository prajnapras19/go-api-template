package routers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/prajnapras19/go-api-template/controllers"
	"gitlab.com/prajnapras19/go-api-template/services"
	"gitlab.com/prajnapras19/go-api-template/repositories"
	
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

func SetupRouter(router *gin.Engine) {
	exampleRepository := repositories.NewExampleRepositories()

	exampleService := services.NewExampleService(exampleRepository)
	
	exampleController := controllers.NewExampleController(exampleService)

	router.GET("/", exampleController.GetExample)
	
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))
}
