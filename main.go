package main

import (
	"os"

	_ "gitlab.com/prajnapras19/go-api-template/docs"

	"github.com/gin-gonic/gin"
	"gitlab.com/prajnapras19/go-api-template/routers"
)


// @title Example API
// @description Just an example API
// @schemes http https
func main() {
	router := gin.Default()
	routers.SetupRouter(router)

	port, exists := os.LookupEnv("PORT")
	if !exists {
		port = "8080"
	}
	router.Run(":" + port)
}
